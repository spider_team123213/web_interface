import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreeDJoystickComponent } from './three-d-joystick.component';

describe('ThreeDJoystickComponent', () => {
  let component: ThreeDJoystickComponent;
  let fixture: ComponentFixture<ThreeDJoystickComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThreeDJoystickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreeDJoystickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
