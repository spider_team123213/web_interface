import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectionStrategy, Output, EventEmitter, Input } from '@angular/core';
import { JoystickComponent } from '../joystick/joystick.component';
import { MatSlider } from '@angular/material/slider';
import { map, startWith } from 'rxjs/operators';
import { combineLatest, Subject } from 'rxjs';

@Component({
  selector: 'three-d-joystick',
  templateUrl: './three-d-joystick.component.html',
  styleUrls: ['./three-d-joystick.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ThreeDJoystickComponent implements AfterViewInit {
  @ViewChild('joystick') joystick: JoystickComponent;
  @ViewChild('slider') slider: MatSlider;

  @Input() coords = {
    x: 0,
    y: 0,
    z: 0,
  };

  @Output() onInput = new EventEmitter();

  constructor() {
    console.log(this.constructor.name, this);
  }


  ngAfterViewInit() {
    this.slider.input.subscribe((v) => {
      this.coords.z = v.value;
      this.onInput.emit(this.coords);
    })
  }

  on2d(dta: { x: number, y: number }) {
    this.coords.x = dta.x;
    this.coords.y = dta.y;
    this.onInput.emit(this.coords);
  }
}
