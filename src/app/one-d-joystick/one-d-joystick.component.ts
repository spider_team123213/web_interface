import { Component, OnInit, ViewChild, Input, Output } from '@angular/core';
import { MatSlider } from '@angular/material/slider';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'one-d-joystick',
  templateUrl: './one-d-joystick.component.html',
  styleUrls: ['./one-d-joystick.component.scss']
})
export class OneDJoystickComponent implements OnInit {
  @ViewChild(MatSlider) slider: MatSlider;
  @Input() vertical;
  @Input() inputClock: Observable<void>;
  @Input() defaultValue: number = 0;
  @Output() onClockInput = new Subject<number>();
  value: number;

  constructor() {
    console.log(this.constructor.name, this);
  }

  ngOnInit() {
    this.inputClock.subscribe(() => {
      if (this.slider._isSliding) {
        this.onClockInput.next(this.slider.value);
      } else if (this.slider.value != this.defaultValue) {
        this.slider.value = this.defaultValue;
      }
    });
  }

}
