import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneDJoystickComponent } from './one-d-joystick.component';

describe('OneDJoystickComponent', () => {
  let component: OneDJoystickComponent;
  let fixture: ComponentFixture<OneDJoystickComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneDJoystickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneDJoystickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
