import { Component, OnInit, ChangeDetectionStrategy, Input, OnDestroy, HostListener } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { distinct, share, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'round-button',
  templateUrl: './round-button.component.html',
  styleUrls: ['./round-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RoundButtonComponent implements OnDestroy {


  state_: Subject<boolean> = new BehaviorSubject(false);
  state = this.state_.pipe(
    distinctUntilChanged(),
    share()
  );


  @Input() background: string;
  @Input() tag: string;

  constructor() { }

  ngOnDestroy() {
    this.state_.next(false);
    this.state_.complete();
  }

  down() {
    this.state_.next(true);
  }
  up() {
    this.state_.next(false);
  }
}
