import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSliderModule } from '@angular/material/slider';
import { AppComponent } from './app.component';
import { JoystickComponent } from './joystick/joystick.component';
import { RoundButtonComponent } from './round-button/round-button.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ThreeDJoystickComponent } from './three-d-joystick/three-d-joystick.component';
import { OneDJoystickComponent } from './one-d-joystick/one-d-joystick.component';



@NgModule({
  declarations: [
    AppComponent,
    JoystickComponent,
    RoundButtonComponent,
    ThreeDJoystickComponent,
    OneDJoystickComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    DragDropModule,
    MatGridListModule,
    MatSliderModule,
    MatDialogModule,
    MatProgressSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
