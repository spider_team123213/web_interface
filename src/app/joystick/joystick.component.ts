import { Component, OnInit, ViewChild, ChangeDetectionStrategy, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'joystick',
  templateUrl: './joystick.component.html',
  styleUrls: ['./joystick.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JoystickComponent implements OnInit {
  @ViewChild('base') base: ElementRef;
  @ViewChild('svg') svg: ElementRef;
  @Input() inputClock: Observable<void>;
  @Input() minX: number = -1;
  @Input() maxX: number = 1;
  @Input() minY: number = -1;
  @Input() maxY: number = 1;

  @Output() onCoords = new EventEmitter<{ x: number, y: number }>();
  @Output() onClockCoords = new Subject<{ x: number, y: number }>();

  constructor() {
    console.log(this.constructor.name, this);
  }

  ngOnInit() {
    this.inputClock.subscribe(() => {
      if (this.isDown) {
        this.onClockCoords.next({
          x: this.left * 2,
          y: this.top * -2,
        });
      } else {
        this.left = 0;
        this.top = 0;
      }
    });
    this.pt = this.svg.nativeElement.createSVGPoint();
  }

  pt;
  left = 0;
  top = 0;

  isDown = false;
  down(e: MouseEvent) {
    this.isDown = true;
    this.move(e);
  }
  up(e: MouseEvent) {
    this.isDown = false;
  }
  move(e: MouseEvent) {
    if (this.isDown) {
      this.pt.x = e.clientX;
      this.pt.y = e.clientY;
      const cursorpt = this.pt.matrixTransform(
        this.svg.nativeElement.getScreenCTM().inverse()
      );
      console.log(cursorpt);

      let x = cursorpt.x;
      let y = cursorpt.y;
      const d = Math.sqrt(x ** 2 + y ** 2);
      if (d > 0.5) {
        x = x * 0.5 / d;
        y = y * 0.5 / d;
      }
      this.left = x;
      this.top = y;
      this.onCoords.emit({ x: x * 2, y: y * -2 });
    }
  }

}
