import {
  Component, ViewChildren, QueryList, OnInit, AfterViewInit, Inject, ViewChild, TemplateRef
} from '@angular/core';
import { MatSlider, MatSliderChange } from '@angular/material/slider';
import { debounceTime, sampleTime } from 'rxjs/operators';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Subject } from 'rxjs';


const servo_count = 24;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements AfterViewInit {
  title = 'web-interface';

  move_input = new Subject<{ x: number, y: number }>();

  data = {
    x: 225,
    y: 0,
    z: -350
  }

  on_input(key, evt) {
    this.data[key] = evt.value;
    this.ws.send(JSON.stringify({
      key: "coords_test",
      x: this.data.x,
      y: this.data.y,
      z: this.data.z
    }));
  }

  ws: WebSocket;
  @ViewChild('disconnectedPopup') disconnectedPopup: TemplateRef<any>;

  public clock = new Subject<void>();
  constructor(
    public dialog: MatDialog,
  ) {
    setInterval(() => this.clock.next(), 150);
  }

  sit_val = -100;

  disconnectedDialog: MatDialogRef<any, any>;
  initWS() {
    if (!this.disconnectedDialog)
      this.disconnectedDialog = this.dialog.open(this.disconnectedPopup, {
        disableClose: true
      });
    this.ws = new WebSocket("ws://192.168.88.190/s");
    this.ws.onclose = () => {
      this.ws.onclose = null;
      delete this.ws;
      console.log("restarting ws");
      setTimeout(() => { this.initWS(); }, 1000);
    };
    this.ws.onmessage = (ev) => {
      console.log(JSON.parse(ev.data));
      /*      const fileReader = new FileReader();
            fileReader.onload = (evt) => {
              const arr = new Uint16Array(fileReader.result as ArrayBuffer);
              for (let i = 0; i < servo_count; i++) {
                this.servos[i].val = arr[i];
              }
            }
            fileReader.readAsArrayBuffer(ev.data);*/
    };
    this.ws.onopen = () => {
      this.disconnectedDialog.close();
      delete this.disconnectedDialog;
    }
  }

  pause() {
    this.ws.send(JSON.stringify({ key: "pause" }));
  }

  sit_input(value: number) {
    try {
      this.ws.send(JSON.stringify({
        key: "sit",
        height: (value + 1) / 2,
      }));
    } catch (err) { console.error(err) }
  }

  turn_input(value: number) {
    try {
      this.ws.send(JSON.stringify({
        key: "turn",
        speed: value,
      }));
    } catch (err) { console.error(err) }
  }
  ngAfterViewInit() {
    var x: MatSlider;
    setTimeout(() => this.initWS(), 0);
    this.move_input.subscribe((d) => {
      try {
        this.ws.send(JSON.stringify({
          key: "move",
          angle: Math.atan2(d.y, d.x),
          speed: Math.hypot(d.y, d.x),
        }));
      } catch (err) { console.error(err) }
    });

    /*    this.sliders.forEach((slider, ix) => {
          slider.input.pipe(
            sampleTime(50)
          ).subscribe((event: MatSliderChange) => {
            const data = {
              n: ix,
              v: event.value
            }
            console.log(data);
            if (this.ws && this.ws.readyState == 1) this.ws.send(JSON.stringify(data));
          });
        });*/
  }



}


