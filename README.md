# Веб-интфейс.

Пока работа на локальном комьюптере. Адрес паука прописывается в файле scr/app/app.component.ts
```
this.ws = new WebSocket("ws://192.168.88.190/s");
```

Для запуска необходимо установить https://nodejs.org/en/ LTS версию, из командной строки установить angular CLI ```npm install @angular/cli```, запустить сервер разработки с помощью ```ng serve```, после чего интерфейс будет доступен по адресу http://localhost:4200/





# WebInterface

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
